import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";

@Component({
  selector: "application-loader", templateUrl: "./loader.component.html", styleUrls: ["./loader.component.scss"]
})
export class LoaderComponent implements OnInit {
  json = "";
  array = [];
  collection = "";

  constructor(private ws: WebsocketService) {
  }

  ngOnInit() {
  }

  load(event) {
    console.log(event.target.files);
    for (let i = 0; i < event.target.files.length; i++) {
      const file = event.target.files[i];
      const fr = new FileReader();
      fr.onload = (e) => {
        this.json = e.target["result"];
      };
      fr.readAsText(file);
    }
  }

  parse() {
    try {
      this.array = JSON.parse(this.json);
    } catch (e) {
      alert(e);
    }
    console.log(this.array);
  }

  clean() {
    this.json = "";
    this.array = [];
  }

  save(collection) {
    try {
      this.array = JSON.parse(this.json);
      collection = collection.trim();
      if (collection !== "") {
        for (let i = 0; i < this.array.length; i++) {
          const wsm = new WsMessage(Math.random());
          wsm.method = "Repository.Save";
          wsm.params = {
            collection: collection, object: this.array[i], token: localStorage.getItem("token")
          };
          this.ws.send(wsm).subscribe(data => {
            console.log(data);
          });
        }
      }
    } catch (e) {
      alert(e);
    }
  }
}

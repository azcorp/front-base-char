import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {environment} from "../environments/environment";

@Injectable()
export class StorageService {
  public url = `${environment.webdav}/`;
  private headers = new Headers({"Authorization": localStorage.getItem("token")});
  private options = new RequestOptions({headers: this.headers});
  constructor(private http: Http) { }

  put(file, url): Observable<any> {
    return new Observable(
      observer => {
        const xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 201 || xhr.status === 204) {
              observer.next(url);
              observer.complete();
            } else {
              console.log(xhr);
            }
          }
        };
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
        xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
        xhr.send(file);
      }
    );
  }

  remove(url): Observable<any> {
    return new Observable(
      observer => {
        const xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 201 || xhr.status === 204) {
              observer.next(url);
              observer.complete();
            } else {
              console.log(xhr);
            }
          }
        };
        xhr.open("DELETE", url, true);
        xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
        xhr.send();
      });
  }
  mkdir(url): Observable<any> {
    return new Observable(
      observer => {
        const xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 201 || xhr.status === 204) {
              observer.next(url);
              observer.complete();
            } else {
              console.log(xhr);
            }
          }
        };
        xhr.open("MKCOL", url, true);
        xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
        xhr.send();
      });
  }
  move(surl, durl) {
    return new Observable(
      observer => {
        const xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.onreadystatechange = () => {

          if (xhr.readyState === 4) {
            if (xhr.status === 201 || xhr.status === 204) {
              observer.next(durl);
              observer.complete();
            } else {
              console.log(xhr);
            }
          }
        };
        xhr.open("MOVE", surl, true);
        xhr.setRequestHeader("Destination", durl);
        xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
        xhr.send();
      });
  }
  getFolder(url): Observable<any> {
   return  this.http.get(url, this.options)
     .map(this.extractData)
     .catch(this.handleError);
  }

  private extractData(res: Response) {
    return res.json();
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
}
export class File {
  name: string;
  mtime: string;
  size: number;
  type: string;
}

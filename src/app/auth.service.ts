import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {environment} from "../environments/environment";
import {Observable} from "rxjs/Observable";
import * as _ from "lodash";

@Injectable()
export class AuthService {
  public token: string;
  private url = `${environment.server}/`;
  private headers = new Headers({"Content-Type": "application/x-www-form-urlencoded"});
  private options = new RequestOptions({headers: this.headers});
  public session: Session;
  public user: User;

  constructor(private http: Http) {
    this.setToken(localStorage.getItem("token"));
    this.setSession(JSON.parse(localStorage.getItem("session")));
    this.setUser(JSON.parse(localStorage.getItem("user")));
    this.login("", "").subscribe(data => {
      console.log(data);
    });
  }

  public setToken(token) {
    if (token == null) {
      this.token = token;
      localStorage.removeItem("token");
    } else {
      localStorage.setItem("token", token);
      this.token = token;
    }
  }
  public setUser(user) {
    console.log(user);
    if (user == null) {
      this.user = user;
      localStorage.removeItem("user");
    } else {
      localStorage.setItem("user", JSON.stringify(user));
      this.user = user;
    }
  }
  public setSession(session) {
    console.log(session);
    if (session == null) {
      this.session = session;
      localStorage.removeItem("session");
    } else {
      localStorage.setItem("session", JSON.stringify(session));
      this.session = session;
    }
  }

  public login(username, password): Observable<any> {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    if (localStorage.getItem("token") !== null) {
      headers = new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": localStorage.getItem("token")
      });
    }
    return this.http.post(this.url + "login", `username=${username}&password=${password}`, new RequestOptions({headers: headers}))
      .map(this.extractData).catch((error: Response) => {
        if (error.status === 401 || 403) {
          console.log(error.status);
          this.logout().subscribe(() => {
            this.setUser(null);
            this.setSession(null);
            this.setToken(null);
          });
        }
        return Observable.throw(error);
      });
  }

  public logout(): Observable<any> {
    return this.http.post(this.url + "logout", this.token, this.options).map(this.extractData);
  }

  private extractData(res: Response) {
    return res.json();
  }

  public hasRole(role): boolean {
    if (this.user !== null) {
      const index = _.findIndex(this.user.roles, (item) => {
        return item === role;
      });
      return index !== -1;
    }
    return false;
  }

}

export class User {
  id: number;
  username: string;
  password: string;
  enabled: boolean;
  roles: string[];
}
export class Role {
  id: number;
  name: string;
  permissions: string[];
}
export class Session {
  user: User;
  source: string;
  permissions: string[];
  expiretime: number;
}

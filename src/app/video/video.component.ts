import {Component, HostListener, Input, OnInit, ViewChild} from "@angular/core";
import * as _ from "lodash";

@Component({
  selector: "application-video", templateUrl: "./video.component.html", styleUrls: ["./video.component.scss"],
})

export class VideoComponent implements OnInit {
  @Input() url;
  @Input() height;
  @Input() width;
  @Input() playlist;
  state = false;
  @ViewChild("player") player: any;

  @HostListener("document:keypress", ["$event"])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === " ") {
      this.play();
    }
  }

  constructor() {
  }

  ngOnInit() {
    this.player.nativeElement.onended = () => {
      this.next();
    };
  }

  play() {
    if (this.state) {
      this.player.nativeElement.pause();
    } else {
      this.player.nativeElement.play();
    }
    this.state = !this.state;
  }

  select(item) {
    this.playlist.forEach(video => {
      video["play"] = false;
    });
    this.url = item.url;
    item["play"] = true;
  }

  next() {
    const index = _.findIndex(this.playlist, (item) => {
      return item["play"] === true;
    });
    if (index < this.playlist.length) {
      this.select(this.playlist[index + 1]);
      this.player.nativeElement.oncanplay = () => {
        this.player.nativeElement.play();
        this.player.nativeElement.oncanplay = null;
      };
    } else {
      this.player.nativeElement.stop();
    }
  }
}

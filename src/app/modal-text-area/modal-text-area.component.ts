import {Component, OnInit} from "@angular/core";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: "application-modal-text-area",
  templateUrl: "./modal-text-area.component.html",
  styleUrls: ["./modal-text-area.component.scss"]
})
export class ModalTextAreaComponent implements OnInit {
  text;
  placeholder;
  constructor(public dialogRef: MdDialogRef<ModalTextAreaComponent>) {
  }

  ngOnInit() {
  }

  submit() {
    this.dialogRef.close(this.text);
  }
}

import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";
import {AuthService} from "../auth.service";
import {environment} from "../../environments/environment";
import {StorageService} from "../storage.service";

@Component({
  selector: "application-modal-gift", templateUrl: "./modal-gift.component.html", styleUrls: ["./modal-gift.component.scss"]
})
export class ModalGiftComponent implements OnInit {
  item;
  goods;

  constructor(private ws: WebsocketService, private auth: AuthService, private storage: StorageService) {
  }

  ngOnInit() {
    this.getGoods(this.item["composition"]);
  }

  getGoods(composition) {
    console.log(composition.length);
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Find";
    wsm.params = {
      collection: "Goods", query: {"id": {"$in": composition}}, limit: 0, sort: "name", token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
      console.log(data.result.length);
      this.goods = data.result;
    });
  }
  setImage(item, event) {
    for (let i = 0; i < event.target.files.length; i++) {
      const file = event.target.files[i];
      this.storage.put(file, `${environment.webdav}/images/${file.name}`).subscribe(() => {
        const wsm = new WsMessage(Math.random());
        item["image"] = `${environment.webdav}/images/${file.name}`;
        wsm.method = "Mongo.Save";
        wsm.params = {
          collection: "Goods", object: item, token: localStorage.getItem("token")
        };
        this.ws.send(wsm).subscribe(data => {
          this.getGoods(this.item["composition"]);
        });
      });
    }
  }
}

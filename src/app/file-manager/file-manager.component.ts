import {Component, OnInit} from "@angular/core";
import {StorageService} from "../storage.service";
import {MdDialog, MdDialogConfig} from "@angular/material";
import {ModalImageComponent} from "../modal-image/modal-image.component";
import {ModalTextComponent} from "../modal-text/modal-text.component";
import {ModalTextAreaComponent} from "../modal-text-area/modal-text-area.component";
import {environment} from "../../environments/environment";
import * as _ from "lodash";
import {ModalVideoComponent} from "../modal-video/modal-video.component";

@Component({
  selector: "application-file-manager", templateUrl: "./file-manager.component.html", styleUrls: ["./file-manager.component.scss"]
})
export class FileManagerComponent implements OnInit {
  path: string[] = [this.storage.url];
  list = [];
  selected = [];
  buffer = [];

  constructor(private storage: StorageService, public dialog: MdDialog) {

  }

  ngOnInit() {
    this.update();
  }

  click(item) {
    if (item.type === "directory") {
      this.inFolder(item);
    } else {
      const ext = this.getExtention(item.name);
      const isText = environment.text_ext.indexOf(ext);
      if (isText !== -1) {
        this.openText(item);
      } else {
        const isImage = environment.image_ext.indexOf(ext);
        if (isImage !== -1) {
          this.openImage(item);
        } else {
          const isVideo = environment.video_ext.indexOf(ext);
          if (isVideo !== -1) {
            this.openVideo(item);
          }
        }
      }
    }
  }

  getExtention(name) {
    const result = name.match(/\.[^.]*$/i);
    let ext;
    if (result !== null) {
      ext = result[0].substr(1, result[0].length - 1).toLowerCase();
    } else {
      ext = "";
    }
    return ext;
  }

  isSelectedHasVideo() {
    let result = false;
    this.selected.forEach(item => {
      const isVideo = environment.video_ext.indexOf(this.getExtention(item.name));
      if (isVideo !== -1) {
        result = true;
        return;
      }
    });
    return result;
  }

  isSelected(item) {
    const index = _.findIndex(this.selected, (o) => {
      return o.name === item.name;
    });
    return index !== -1;
  }

  isBuffer(item) {
    const index = _.findIndex(this.buffer, (o) => {
      return o.name === item.name;
    });
    return index !== -1;
  }

  select(item, event, index) {
    if (event.buttons === 1 || event.metaKey) {
      if (!this.isSelected(item)) {
        this.selected.push(item);
      }
    }
    if (event.type === "click") {
      if (!event.shiftKey) {
        this.selected = [];
      } else {
        if (this.selected.length > 0) {
          const a = this.list.indexOf(_.last(this.selected));
          const b = this.list.indexOf(item);
          this.selected = [];
          let delta = [];
          if (a < b) {
            delta = this.list.slice(a, b + 1);
          } else {
            delta = this.list.slice(b, a + 1);
          }
          console.log(delta);
          delta.forEach(t => {
            this.selected.push(t);
          });
        } else {
          this.selected.push(item);
        }
      }
      if (!this.isSelected(item)) {
        this.selected.push(item);
      }
    }
  }

  inFolder(item) {
    if (item.type === "directory") {
      this.path.push(item.name + "/");
      this.update();
    }
  }

  toFolder(index) {
    this.path = this.path.slice(0, index + 1);
    this.update();
  }

  upload(event) {
    console.log(event.target.files);
    for (let i = 0; i < event.target.files.length; i++) {
      const file = event.target.files[i];
      this.storage.put(file, this.path.join("") + file.name).subscribe(() => {
        this.update();
      });
    }
  }

  remove(item) {
    if (item.type === "file") {
      this.storage.remove(this.path.join("") + item.name).subscribe(() => {
        this.update();
      });
    } else {
      this.storage.remove(this.path.join("") + item.name + "/").subscribe(() => {
        this.update();
      });
    }
  }

  cut(item) {
    this.buffer.push(item);
  }

  cutSelected() {
    this.selected.forEach((item) => {
      if (!this.isBuffer(item)) {
        if (item.type === "file") {
          item["url"] = this.path.join("") + item.name;
        } else {
          item["url"] = this.path.join("") + item.name + "/";
        }
        this.buffer.push(item);
      }
    });
    this.selected = [];
  }

  pasteBuffer() {
    this.buffer.forEach((item) => {
      let durl;
      let surl;
      if (item.type === "file") {
        durl = this.path.join("") + item.name;
        surl = item.url;
      } else {
        durl = this.path.join("") + item.name + "/";
        surl = item.url;
      }
      this.storage.move(surl, durl).subscribe(() => {
        this.update();
      });
    });
    this.buffer = [];
  }

  removeSelected() {
    if (this.selected.length > 0) {
      this.selected.forEach((item) => {
        this.remove(item);
      });
    }
  }

  mkdir() {
    const dialogRef = this.dialog.open(ModalTextComponent);
    dialogRef.componentInstance.placeholder = "New folder name";
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.storage.mkdir(this.path.join("") + result + "/").subscribe(() => {
          this.update();
        });
      }
    });
  }

  rename(item, field, placeholder) {
    const dialogRef = this.dialog.open(ModalTextComponent);
    dialogRef.componentInstance.text = item[field];
    dialogRef.componentInstance.placeholder = placeholder;
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        let durl;
        let surl;
        if (item.type === "file") {
          durl = this.path.join("") + result;
          surl = this.path.join("") + item.name;
        } else {
          durl = this.path.join("") + result + "/";
          surl = this.path.join("") + item.name + "/";
        }
        this.storage.move(surl, durl).subscribe(() => {
          this.update();
        });
      }
    });
  }


  openImage(item) {
    let url = this.path.join("") + item.name;
    url = url.replace(/ /g, "%20");
    const dialogConfig = new MdDialogConfig();
    dialogConfig.height = "80%";
    dialogConfig.width = "80%";
    const dialogRef = this.dialog.open(ModalImageComponent, dialogConfig);
    dialogRef.componentInstance.url = url;
  }

  openText(item) {
    if (item.size <= 100000) {
      const dialogConfig = new MdDialogConfig();
      dialogConfig.height = "80%";
      dialogConfig.width = "80%";
      const dialogRef = this.dialog.open(ModalTextAreaComponent, dialogConfig);
      const storage = this.storage;
      const path = this.path;
      let url = this.path.join("") + item.name;
      url = url.replace(/ /g, "%20");
      const rawFile = new XMLHttpRequest();
      rawFile.open("GET", url, true);
      rawFile.setRequestHeader("Authorization", localStorage.getItem("token"));
      rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
          const raw = rawFile.responseText;
          dialogRef.componentInstance.text = raw;
          dialogRef.afterClosed().subscribe(result => {
            if (result !== undefined) {
              storage.put(result, path.join("") + item.name).subscribe(() => {
              });
            }
          });
        }
      };
      rawFile.send();
    }
  }

  openVideo(video) {
    const dialogConfig = new MdDialogConfig();
    dialogConfig.height = "80%";
    dialogConfig.width = "80%";

    if (video === undefined) {

      const list = [];
      this.selected.forEach((item) => {
        const isVideo = environment.video_ext.indexOf(this.getExtention(item.name));
        if (isVideo !== -1) {
          let url = this.path.join("") + item.name;
          url = url.replace(/ /g, "%20");
          item["url"] = url;
          list.push(item);
        }
      });
      const dialogRef = this.dialog.open(ModalVideoComponent, dialogConfig);
      dialogRef.componentInstance.playlist = list;

    } else {
      let url = this.path.join("") + video.name;
      url = url.replace(/ /g, "%20");
      video["url"] = url;
      const dialogRef = this.dialog.open(ModalVideoComponent, dialogConfig);
      dialogRef.componentInstance.playlist = [video];
    }
  }

  update() {
    this.storage.getFolder(this.path.join("")).subscribe(data => {
      this.list = data;
    });
  }
}



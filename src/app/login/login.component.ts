import {Component, OnInit} from "@angular/core";
import {AuthService} from "../auth.service";
import {Observable} from "rxjs/Observable";
import {Router} from "@angular/router";

@Component({
  selector: "application-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  username;
  password;

  constructor(public auth: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.auth.login(this.username, this.password).catch(this.handleError).subscribe(data => {
      console.log(data);
      this.auth.setToken(data["token"]);
      this.auth.setSession(data["session"]);
      this.auth.setUser(data["user"]);
    });
  }

  logout() {
    this.auth.logout().catch(this.handleError).subscribe(data => {
      this.auth.setToken(null);
      this.auth.setSession(null);
      this.auth.setUser(null);
      this.router.navigate(["/"]);
    });
  }
  private handleError(error: Response | any) {
    if (error.status === 401) {
      alert("Wrong credentials");
    }
    return Observable.throw(error);
  }

}

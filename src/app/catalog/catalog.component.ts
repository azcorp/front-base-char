import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";
import {AuthService} from "../auth.service";
import {StorageService} from "../storage.service";
import {environment} from "../../environments/environment";
import {MdDialog, MdDialogConfig} from "@angular/material";
import {ModalGiftComponent} from "../modal-gift/modal-gift.component";

@Component({
  selector: "application-catalog", templateUrl: "./catalog.component.html", styleUrls: ["./catalog.component.scss"]
})
export class CatalogComponent implements OnInit {
  collection;
  name = "";
  limit = 50;

  constructor(private ws: WebsocketService, private auth: AuthService, private storage: StorageService, public dialog: MdDialog) {
  }

  ngOnInit() {
    this.getCollection();
  }

  getCollection() {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Find";
    wsm.params = {
      collection: "Gifts",
      query: {"name": {"$regex": this.name, "$options": "i"}},
      limit: this.limit,
      sort: "name",
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
      this.collection = data.result;
    });
  }

  setImage(item, event) {
    for (let i = 0; i < event.target.files.length; i++) {
      const file = event.target.files[i];
      this.storage.put(file, `${environment.webdav}/images/${file.name}`).subscribe(() => {
        const wsm = new WsMessage(Math.random());
        item["image"] = `${environment.webdav}/images/${file.name}`;
        wsm.method = "Mongo.Save";
        wsm.params = {
          collection: "Gifts", object: item, token: localStorage.getItem("token")
        };
        this.ws.send(wsm).subscribe(data => {
          this.getCollection();
        });
      });
    }
  }
  open(item) {
    const dialogConfig = new MdDialogConfig();
    dialogConfig.height = "85%";
    dialogConfig.width = "85%";
    const dialogRef = this.dialog.open(ModalGiftComponent, dialogConfig);
    dialogRef.componentInstance.item = item;
  }
}

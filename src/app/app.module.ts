import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {WebsocketService} from "./websocket.service";
import {AuthService} from "./auth.service";
import {LoginComponent} from "./login/login.component";
import {FileManagerComponent} from "./file-manager/file-manager.component";
import {StorageService} from "./storage.service";
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdCardModule,
  MdDialogModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdProgressSpinnerModule,
  MdSlideToggleModule,
  MdTabsModule,
  MdToolbarModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MainComponent} from "./main/main.component";
import {RoutingModule} from "../router/router.module";
import {NavPanelComponent} from "./nav-panel/nav-panel.component";
import {UserManagerComponent, UserManagerCreateDialogComponent, UserManagerDialogComponent} from "./user-manager/user-manager.component";
import {RoleManagerComponent} from "./role-manager/role-manager.component";
import {ModalTextComponent} from "./modal-text/modal-text.component";
import {ModalTextAreaComponent} from "./modal-text-area/modal-text-area.component";
import {ImageComponent} from "./image/image.component";
import {MongoManagerComponent} from "./mongo-manager/mongo-manager.component";
import {ModalImageComponent} from "./modal-image/modal-image.component";
import {VideoComponent} from "./video/video.component";
import {ModalVideoComponent} from "./modal-video/modal-video.component";
import {LoaderComponent} from "./loader/loader.component";
import {CatalogComponent} from "./catalog/catalog.component";
import {ModalGiftComponent} from "./modal-gift/modal-gift.component";
import {ContactsComponent} from "./contacts/contacts.component";
import {DeliveryComponent} from "./delivery/delivery.component";
import {BucketComponent} from "./bucket/bucket.component";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FileManagerComponent,
    MainComponent,
    NavPanelComponent,
    UserManagerComponent,
    UserManagerDialogComponent,
    UserManagerCreateDialogComponent,
    RoleManagerComponent,
    ModalTextComponent,
    ModalTextAreaComponent,
    ImageComponent,
    MongoManagerComponent,
    ModalImageComponent,
    VideoComponent,
    ModalVideoComponent,
    LoaderComponent,
    CatalogComponent,
    ModalGiftComponent,
    ContactsComponent,
    DeliveryComponent,
    BucketComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RoutingModule,
    MdButtonModule,
    MdListModule,
    MdIconModule,
    MdMenuModule,
    MdGridListModule,
    MdDialogModule,
    MdInputModule,
    MdToolbarModule,
    MdCardModule,
    MdProgressSpinnerModule,
    MdSlideToggleModule,
    MdAutocompleteModule,
    MdTabsModule
  ],
  entryComponents: [
    UserManagerDialogComponent,
    UserManagerCreateDialogComponent,
    ModalTextComponent,
    ModalTextAreaComponent,
    ModalImageComponent,
    ModalVideoComponent,
    ModalGiftComponent
  ],
  providers: [
    WebsocketService,
    AuthService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

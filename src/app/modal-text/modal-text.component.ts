import {Component, OnInit} from "@angular/core";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: "application-modal-text",
  templateUrl: "./modal-text.component.html",
  styleUrls: ["./modal-text.component.scss"]
})
export class ModalTextComponent implements OnInit {
  text;
  placeholder;
  constructor(public dialogRef: MdDialogRef<ModalTextComponent>) {
  }

  ngOnInit() {
  }

  submit() {
    this.dialogRef.close(this.text);
  }
}

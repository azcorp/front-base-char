import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";

@Component({
  selector: "application-mongo-manager",
  templateUrl: "./mongo-manager.component.html",
  styleUrls: ["./mongo-manager.component.scss"]
})
export class MongoManagerComponent implements OnInit {
  collections = [];
  documents = [];
  collection = {};
  document = {};
  documentJson = "";
  field = "";
  value = "";
  limit = 100;
  sort = "_id";


  constructor(private ws: WebsocketService) {
  }

  ngOnInit() {
    this.getCollections();
  }

  getCollections() {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.GetCollection";
    wsm.params = {
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        data.result.forEach((line) => {
          const wsm2 = new WsMessage(Math.random());
          wsm2.method = "Repository.GetIndex";
          wsm2.params = {
            token: localStorage.getItem("token"),
            collection: line
          };
          this.ws.send(wsm2).subscribe(dataIndex => {
              this.collections.push({name: line, index: dataIndex.result});
            }
          );
        });
      }
    );
  }

  getDocuments() {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Find";
    const query = {};
    if (this.field !== "" && this.value !== "") {
      query[this.field] = {"$regex": this.value, "$options": "i"};
    }
    wsm.params = {
      collection: this.collection["name"],
      query: query,
      limit: this.limit,
      sort: this.sort,
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        this.documents = data.result;
      }
    );
  }

  save(json) {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Save";
    wsm.params = {
      collection: this.collection["name"],
      query: {},
      limit: this.limit,
      sort: this.sort,
      object: JSON.parse(json),
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        this.getDocuments();
      }
    );
  }

  selectCollection(item) {
    this.collection = item;
    this.getDocuments();
  }

  stringify(obj): string {
    return JSON.stringify(obj, null, 2);
  }


}

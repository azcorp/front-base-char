import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";
import {User} from "../auth.service";
import {MdDialog, MdDialogRef} from "@angular/material";

@Component({
  selector: "application-user-manager",
  templateUrl: "./user-manager.component.html",
  styleUrls: ["./user-manager.component.scss"]
})
export class UserManagerComponent implements OnInit {
  userlist = [];
  limit = 10;
  username = ".*";
  rolename = "";
  rolelist = [];


  constructor(private ws: WebsocketService, public dialog: MdDialog) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Find";
    wsm.params = {
      collection: "user",
      query: {"username": {"$regex": this.username, "$options": "i"}},
      limit: this.limit,
      sort: "username",
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        this.userlist = data.result;
      }
    );
  }

  remove(item) {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Delete";
    wsm.params = {
      collection: "user",
      object: item,
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        console.log(data);
        this.getUsers();
      }
    );
  }

  edit(item) {
    const dialogRef = this.dialog.open(UserManagerDialogComponent);
    dialogRef.componentInstance.user = item;
    dialogRef.afterClosed().subscribe(result => {
      this.save(result);
    });
  }

  save(item) {
    console.log(item);
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Save";
    if (!(item.roles instanceof Array)) {
      item.roles = item.roles.split(",");
    }
    wsm.params = {
      collection: "user",
      object: item,
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
      console.log(data);
      this.getUsers();
    });
  }

  createUser() {
    this.dialog.open(UserManagerCreateDialogComponent).afterClosed().subscribe(result => {
        if (result !== undefined) {
          result.roles = result.roles.split(",");
          this.save(result);
        }
      }
    );
  }
}
@Component({
  selector: "application-user-manager-dialog",
  template: `
    <p>Id: {{user?._id}}</p>
    <div class="column">
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.username" placeholder="Username">
      </md-input-container>
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.password" placeholder="Password">
      </md-input-container>
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.roles" placeholder="Roles">
      </md-input-container>
      <md-slide-toggle [(ngModel)]="user.enabled">Enabled</md-slide-toggle>
      <button md-raised-button (click)="submit()">Apply</button>
    </div>

  `
})
export class UserManagerDialogComponent {
  user;

  constructor(public dialogRef: MdDialogRef<UserManagerDialogComponent>, private ws: WebsocketService) {
  }

  submit() {
    this.dialogRef.close(this.user);
  }
}

@Component({
  selector: "application-user-manager-create-dialog",
  template: `
    <div class="column">
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.username" placeholder="Username">
      </md-input-container>
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.password" placeholder="Password">
      </md-input-container>
      <md-input-container>
        <input mdInput (keyup.enter)="submit()" [(ngModel)]="user.roles" placeholder="Roles">
      </md-input-container>
      <md-slide-toggle [(ngModel)]="user.enabled">Enabled</md-slide-toggle>
      <button md-raised-button (click)="submit()">Create</button>
    </div>


  `
})
export class UserManagerCreateDialogComponent {
  user: User = new User();

  constructor(public dialogRef: MdDialogRef<UserManagerCreateDialogComponent>) {
  }

  submit() {
    this.dialogRef.close(this.user);
  }
}

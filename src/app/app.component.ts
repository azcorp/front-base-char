import {Component} from "@angular/core";
import {AuthService} from "./auth.service";
import {WebsocketService} from "./websocket.service";

@Component({
  selector: "application-root",
  template: `
    <div style="width: 100%;height: 100%; margin: 0; padding: 0">
        <application-nav-panel></application-nav-panel>
        <router-outlet></router-outlet>
    </div>`,
  styles: []
})
export class AppComponent {
  constructor(private ws: WebsocketService, private auth: AuthService) {
  }
}

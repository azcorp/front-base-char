import {Injectable} from "@angular/core";
import * as Rx from "rxjs/Rx";
import {environment} from "../environments/environment";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";

@Injectable()
export class WebsocketService {
  public isConnected = false;
  private url = `${environment.websocket}`;
  private ws;
  private stack: Map<string, Observer<any>> = new Map();
  private subject: Rx.Subject<MessageEvent>;
  constructor() {
    this.init();
  }
  public init() {
    this.ws = <Rx.Subject<any>>this.connect(this.url).map((response: MessageEvent): WsMessage => {
      return JSON.parse(response.data) as WsMessage;
    });
    this.ws.asObservable().subscribe(msg => {
      this.stack.get(msg.id).next(msg);
      this.stack.get(msg.id).complete();
      this.stack.delete(msg.id);
    });
  }

  private connect(url): Rx.Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
    }
    return this.subject;
  }

  private create(url): Rx.Subject<MessageEvent> {
    const ws = new WebSocket(url);
    const observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
      ws.onopen = () => {
        this.isConnected = true;
        console.log("WebSocket open");
      };
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    }).share();

    const observer = {
      next: (data: WsMessage) => {
        console.log(data, "send to", this.url);
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        } else {
          console.log("WebSocket is closed. Reconnecting...");
          this.init();
        }
      },
      error: (error) => {
        this.isConnected = false;
        console.log("WebSocket error. Reconnecting...");
        this.init();
      },
      complete: () => {
        this.isConnected = false;
        console.log("WebSocket close");
      }
    };
    return Rx.Subject.create(observer, observable);
  }

  send(msg: WsMessage): Observable<any> {
    const observable = new Observable(observer => {
      this.stack.set(msg.id, observer);
    });
    this.ws.next(msg);
    return observable;
  }
}

export class WsMessage {
  id: string;
  method: string;
  params: any;
  result: any;
  error: any;
  jsonrpc: string;

  constructor(id) {
    this.id = id;
    this.jsonrpc = "2.0";
  }
}

import {Component, OnInit} from "@angular/core";
import {AuthService} from "../auth.service";

@Component({
  selector: "application-nav-panel",
  templateUrl: "./nav-panel.component.html",
  styleUrls: ["./nav-panel.component.scss"]
})
export class NavPanelComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}

import {Component, OnInit} from "@angular/core";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: "application-modal-video", templateUrl: "./modal-video.component.html", styleUrls: ["./modal-video.component.scss"]
})
export class ModalVideoComponent implements OnInit {
  playlist;
  url;

  constructor(public dialogRef: MdDialogRef<ModalVideoComponent>) {
  }

  ngOnInit() {
    this.playlist[0]["play"] = true;
  }

  close() {
    this.dialogRef.close(this.playlist);
  }
}

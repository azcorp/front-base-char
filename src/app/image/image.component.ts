import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: "application-image",
  templateUrl: "./image.component.html",
  styleUrls: ["./image.component.scss"]
})
export class ImageComponent implements OnInit {
  @Input() url;
  @Input() height;
  @Input() width;
  constructor() { }

  ngOnInit() {
  }

}

import {Component, OnInit} from "@angular/core";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: "application-modal-image",
  templateUrl: "./modal-image.component.html",
  styleUrls: ["./modal-image.component.scss"]
})
export class ModalImageComponent implements OnInit {
  url;
  constructor(public dialogRef: MdDialogRef<ModalImageComponent>) {
  }

  ngOnInit() {
  }

  submit() {
    this.dialogRef.close(this.url);
  }
}

import {Component, OnInit} from "@angular/core";
import {WebsocketService, WsMessage} from "../websocket.service";
import {MdDialog} from "@angular/material";
import {ModalTextComponent} from "../modal-text/modal-text.component";
import {Role} from "../auth.service";

@Component({
  selector: "application-role-manager",
  templateUrl: "./role-manager.component.html",
  styleUrls: ["./role-manager.component.scss"]
})
export class RoleManagerComponent implements OnInit {
  limit = 10;
  rolename = ".*";
  rolelist = [];

  constructor(private ws: WebsocketService, public dialog: MdDialog) {
  }

  ngOnInit() {
    this.getRoles();
  }

  getRoles() {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Find";
    wsm.params = {
      query: {"name": {"$regex": this.rolename, "$options": "i"}},
      limit: this.limit,
      sort: "name",
      collection: "role",
      token: localStorage.getItem("token")
    };
    this.ws.send(wsm).subscribe(data => {
        this.rolelist = data.result;
      }
    );
  }

  create() {
    const role = new Role();
    role.permissions = ["Find"];
    role.name = "New Role";
    this.save(role);
  }

  edit(item, field, placeholder) {
    const dialogRef = this.dialog.open(ModalTextComponent);
    dialogRef.componentInstance.text = item[field];
    dialogRef.componentInstance.placeholder = placeholder;
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        item[field] = result;
        this.save(item);
      }

    });

  }

  save(item) {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Save";
    console.log(item);
    if (!(item.permissions instanceof Array)) {
      item.permissions = item.permissions.split(",");
    }
    wsm.params = {
      token: localStorage.getItem("token"),
      collection: "role",
      object: item
    };
    this.ws.send(wsm).subscribe(data => {
        console.log(data);
        this.getRoles();
      }
    );
  }

  remove(item) {
    const wsm = new WsMessage(Math.random());
    wsm.method = "Mongo.Delete";
    wsm.params = {
      token: localStorage.getItem("token"),
      collection: "role",
      object: item
    };
    this.ws.send(wsm).subscribe(data => {
        console.log(data);
        this.getRoles();
      }
    );
  }
}

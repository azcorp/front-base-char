import {Injectable} from "@angular/core";

@Injectable()
export class BucketServiceService {
  bucket = new Bucket();

  constructor() {
    if (localStorage.getItem("bucket") != null) {
      this.pullBucketFromStorage();
    }
  }

  pushBucketToStorage() {
    localStorage.setItem("bucket", JSON.stringify(this.bucket));
  }

  pullBucketFromStorage() {
    this.bucket = JSON.parse(localStorage.getItem("bucket"));
  }

  recalcBucket() {
    this.pushBucketToStorage();
  }

  clearBucket() {
    this.bucket = new Bucket();
    this.recalcBucket();
  }

}
export class Bucket {
}

/**
 * Created by az on 19.04.17.
 */
import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor() {
  }

  canActivate() {
    return localStorage.getItem("token") !== null;
  }
}

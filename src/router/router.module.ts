/**
 * Created by az on 19.04.17.
 */
import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MainComponent} from "../app/main/main.component";
import {AuthGuard} from "./auth-guard.service";
import {FileManagerComponent} from "../app/file-manager/file-manager.component";
import {UserManagerComponent} from "../app/user-manager/user-manager.component";
import {RoleManagerComponent} from "../app/role-manager/role-manager.component";
import {MongoManagerComponent} from "../app/mongo-manager/mongo-manager.component";
import {WSGuard} from "./ws-guard.service";
import {LoaderComponent} from "../app/loader/loader.component";
import {CatalogComponent} from "../app/catalog/catalog.component";
import {ContactsComponent} from "../app/contacts/contacts.component";
import {DeliveryComponent} from "app/delivery/delivery.component";
import {BucketComponent} from "../app/bucket/bucket.component";

const routes: Routes = [
  {path: "", redirectTo: "main", pathMatch: "full"},
  { path: "main",
    component: MainComponent,
    children: [
      { path: "file-manager", component: FileManagerComponent  },
      { path: "user-manager", component: UserManagerComponent, canActivate: [WSGuard, AuthGuard] },
      { path: "mongo-manager", component: MongoManagerComponent, canActivate: [WSGuard, AuthGuard] },
      { path: "role-manager", component: RoleManagerComponent, canActivate: [WSGuard, AuthGuard] },
      { path: "loader", component: LoaderComponent, canActivate: [WSGuard, AuthGuard] },
      { path: "catalog", component: CatalogComponent, canActivate: [WSGuard] },
      { path: "contacts", component: ContactsComponent},
      { path: "delivery", component: DeliveryComponent},
      { path: "bucket", component: BucketComponent, canActivate: [WSGuard]}
      ]}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, WSGuard]
})
export class RoutingModule {
}

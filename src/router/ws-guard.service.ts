/**
 * Created by az on 19.04.17.
 */
import {Inject, Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {WebsocketService} from "../app/websocket.service";

@Injectable()
export class WSGuard implements CanActivate {
  constructor(@Inject(WebsocketService)private ws: WebsocketService) {
  }

  canActivate(): Promise<any> {
   return new Promise((resolve) => {
      const counter = setInterval(() => {
        console.log("wait");
        if (this.ws.isConnected) {
          resolve(true);
          clearInterval(counter);
        }
      }, 1);
    }).then(() => {
      return this.ws.isConnected;
    });
  }
}
